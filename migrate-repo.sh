#!/bin/bash
###
# Copyright (c) 2016 thomas.zink_at_uni-konstanz_dot_de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

set -e -o pipefail
shopt -s failglob

usage() {
	echo "usage: `basename $0` <old-origin> <new-origin>"
	echo ""
	echo "Migrate git repo to new server."
	exit 0;
}

# check parameters
[[ -z "$1" ]] && usage;
[[ -z "$2" ]] && usage;

# set variables
OLD="$1"
NEW="$2"
REPO="${OLD##*/}"
REPO="${REPO%.git}"
REPN="${NEW##*/}"
REPN="${REPN%.git}"

# do it
echo "migrating ${REPO} from ${OLD} to ${NEW} ..."

git clone "${OLD}"
cd ${REPO}
git fetch --all
git fetch --tags
git remote rename origin oldorigin
git remote add origin "${NEW}"
git push origin --all
git push origin --tags
git remote rm oldorigin
cd ..

mv -v "${REPO}" "${REPN}.migrated"

echo "done"
