#!/bin/bash
###
# Copyright (c) 2016 thomas.zink_at_uni-konstanz_dot_de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###

# create_git_repo.sh
#
# Creates, initializes a new git repo and fixes user:group permissions
# usage: create_git_repo [repo-name]

set -e -o pipefail
shopt -s failglob

usage() {
	echo "usage: $0 <dir> <repo-name>";
	exit 0;
}

# check parameters
[[ -z "$1" ]] && usage;
[[ -z "$2" ]] && usage;



GITUSER="git"
GITGROUP="$GITUSER"
GITPATH="$1"
REPO="$2"

check_user_group() {
	if [ -z "$(getent passwd ${GITUSER})" ]; then
		echo "WARN: user $GITUSER does not exist. Creating user.";
		useradd -r -s /bin/false ${GITUSER};
	fi

	if [ -z "$(getent group ${GITGROUP})" ]; then
		echo "WARN: group $GITGROUP does not exist. Creating group.";
		groupadd $GITGROUP;
	fi	
}

create_git_repo() {
	dir=${GITPATH}/${1}.git
	echo "Creating new Repo ${dir}"
	mkdir -p ${dir}
	git init --bare ${dir}
	chown -R ${GITUSER}:${GITGROUP} ${dir}
	chmod -R 770 ${dir}
	for i in config description HEAD; do
		chmod 660 ${dir}/$i;
	done;
}

check_user_group;
create_git_repo $REPO;
